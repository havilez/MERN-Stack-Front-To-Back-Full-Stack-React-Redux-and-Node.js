import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DatabaseModule } from '../database/database.module';
import { UserProviders } from './providers/users.provider';
// import config from '../config/keys';
// import { UserSchema } from './schema/User.schema';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';

@Module({
    imports: [
      DatabaseModule,
    ],
    providers: [...UserProviders, UsersService],
    exports: [ ...UserProviders],
    controllers: [UsersController],

  })
export class UsersModule {}
