import { Controller, Get } from '@nestjs/common';

@Controller('/api/posts')
export class PostsController {
    @Get()
    findAll(): string {
        return 'find all posts';
    }
}
