import { Module } from '@nestjs/common';
import { ProfileController } from './profilecontroller';

@Module({
  controllers: [ProfileController ],
})
export class ProfileModule {}
