import { Controller, Get } from '@nestjs/common';

@Controller('/api/profile')
export class ProfileController {
    @Get()
    findAll(): string {
        return 'findAll profiles called';
    }
}
